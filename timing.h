#ifndef _TIMING_H_
#define _TIMING_H_

#include <iostream>
#include <time.h>

using std::endl;
using std::cout;

//
// Get the current time as a timespec struct.
// Has nanosecond precision.
//
timespec get_time      (void);

//
// Return the difference in two timespec structs as a normalized double.
//
double   get_time_diff (timespec, timespec);

#endif
