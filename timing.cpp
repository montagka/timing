#include "timing.h"

//
// Get the time in second and nanosecond resolution.
//
timespec get_time (){
  timespec time1;

  clock_gettime ((clockid_t) CLOCK_REALTIME, &time1);

  return time1;
}

//
// Calculate the time difference in seconds (with nanosecond precision) of
// the two timespecs.
//
double get_time_diff (timespec time1, timespec time2){
  double time, nanoseconds;
  
  //
  // Get the difference in seconds.
  //
  time = difftime (time2.tv_sec, time1.tv_sec);

  //
  // Get the difference in nanoseconds and add it to the time.
  //
  nanoseconds = (double)(time2.tv_nsec - time1.tv_nsec);
  nanoseconds /= (double) 1000000000;
  time += nanoseconds;

  return time;
}


